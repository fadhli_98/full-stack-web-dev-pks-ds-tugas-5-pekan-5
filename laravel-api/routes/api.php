<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user' , function () {
   return auth()->user();
});

#route resource post
Route::apiResource('/post', 'PostController');

#route resource role
Route::apiResource('/role', 'RoleController');

#route resource comment
Route::apiResource('/comment', 'CommentController');


Route::group(['prefix' => 'auth', 'namespace'=> 'auth'], function() {

    Route::post('/register', 'RegisterController')->name('auth.register');

    Route::post('/regenerate-otp-code', 'RegenerateOtpController')->name('auth.generate');

    Route::post('/verification', 'VerificationController')->name('auth.verification');

    Route::post('/update-password', 'UpdatePasswordnController')->name('auth.update_password');

    Route::post('/login', 'LoginController')->name('auth.login');
});
